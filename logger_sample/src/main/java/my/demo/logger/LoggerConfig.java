
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.demo.logger;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LoggerConfig {

   public static Logger config(@SuppressWarnings("all") Class clazz) 
   {
    // About: This method returns a fully configured logger object which can be used for logging by any method 
    // or any class/object.

    // Step 1: Configuring both console and file output.....
    // Credit: https://stackoverflow.com/questions/8943661/please-initialize-the-log4j-system-properly-warning

    /*
       [Note:] For output pattern, refer this note:
       --------------------------------------------

        %d -> Date and Time
        %t -> Method Name
        %p -> Message Catagory
        %m -> Message
        %n -> New Line

        For date and time:
        d -> Day
        M -> Month
        y -> Year
        H -> Hour
        m-> Minute(s)
        s-> Second(s)
    */

    String outputConversionPattern = "%nDate:[%d{dd MMM yyyy}]%nTime:[%d{HH:mm:ss}]" 
                                   + "%nMethod:[%t]%nMessage catagory:[%p]%nMessage:[%m]%n%n"; // Output Conversion pattern
    Properties loggerSettings = new Properties();
    // Root Logger Configuration
    loggerSettings.setProperty("log4j.rootLogger", "ALL, stdout, logfile");
    // Console Output Configurations
    loggerSettings.setProperty("log4j.appender.stdout", "org.apache.log4j.ConsoleAppender");
    loggerSettings.setProperty("log4j.appender.stdout.layout", "org.apache.log4j.PatternLayout");
    loggerSettings.setProperty("log4j.appender.stdout.layout.ConversionPattern", outputConversionPattern);
    // File Output Configurations
    loggerSettings.setProperty("log4j.appender.logfile", "org.apache.log4j.FileAppender");
    loggerSettings.setProperty("log4j.appender.logfile.layout", "org.apache.log4j.PatternLayout");
    loggerSettings.setProperty("log4j.appender.logfile.layout.ConversionPattern", outputConversionPattern);
    loggerSettings.setProperty("log4j.appender.logfile.File", "loggings.txt");
    // Adding the configurations to logger.
    PropertyConfigurator.configure(loggerSettings);
    
    // Step 2: Creating a logger object.
    Logger logger = Logger.getLogger(clazz);
    
    // Step 3: Returning the logger object.
    return logger;
   }
}
