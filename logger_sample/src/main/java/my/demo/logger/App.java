
// Author: Abadhesh Mishra [Employee Id: 8117322]

package my.demo.logger;

import org.apache.log4j.Logger;

// Testing the loggings.
public class App 
{
    private static Logger logger = LoggerConfig.config(App.class);
    // Testing the loggings here.
    public static void main(String[] args)
    {
        logger.info("Sample Information Message"); // Info Message
        logger.warn("Sample Warning Message"); // Warning Message
        logger.error("Sample Error Message"); // Error Message
        logger.fatal("Sample Fatal Error Message"); // Fatal Error Message
        logger.trace("Sample Trace Message"); // Trace Message
        logger.debug("Sample Debug Message"); // Debug Message
    }
}
