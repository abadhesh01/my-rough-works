
// Author: Abadhesh Mishra [Employee Id: 8117322]

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class Fast2SMSapi {

    static {

        /*
             About Code:
             -----------
             This piece of code disables security certificate verification for any URL.
         */

        // Credit:
        // https://stackoverflow.com/questions/2752266/make-a-connection-to-a-https-server-from-java-and-ignore-the-validity-of-the-sec

        // Create a trust manager that does not validate certificate chains
        TrustManager[] trustAllCerts = new TrustManager[] {
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        // Install the all-trusting trust manager
        try {
            SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
        } catch (Exception e) {

        }

        // Now you can access an https URL without having the certificate in the
        // truststore.
    }

    public static void main(String[] args) {

        // Scanner object to take keyboard input.
        java.util.Scanner scanner = new java.util.Scanner(System.in);

        // API key to send SMS.
        String apiKey = "auWg6ytosEKE6JNUIQYlu6mzQNLVFx5P4YegrZfyIB8BRZQmb30tj7GPbyLL";

        // Input:
        System.out.print("Enter your message: ");
        String message = scanner.nextLine(); // Input: Message
        System.out.print("Enter the mobile number: ");
        String mobileNumber = scanner.nextLine(); // Input: Mobile Number
        System.out.println("Message: \"" + message + "\"");
        System.out.println("Mobile Number: \"" + mobileNumber + "\"");

        // Encoding the given message.
        try {
            message = URLEncoder.encode(message, "UTF-8");
        } catch (UnsupportedEncodingException error) {
            System.out.println("Error:[" + error.getClass().getName() + "]: " + error.getMessage());
            System.exit(0);
        }
        // System.out.println("Encoded Message: \"" + message + "\"");

        // Generating the request URL.
        String requestURL = "https://www.fast2sms.com/dev/bulkV2?authorization=" + apiKey
                + "&message=" + message
                + "&language=english&route=q&numbers=" + mobileNumber;
        // System.out.println("Request URL: " + requestURL);

        // Sending the request URL.
        URL url = null;
        HttpURLConnection connection = null;

        try {
            url = new URL(requestURL);
        } catch (MalformedURLException error) {
            System.out.println("Error:[" + error.getClass().getName() + "]: " + error.getMessage());
            System.exit(0);
        }

        try {
            connection = (HttpURLConnection) url.openConnection();
        } catch (IOException error) {
            System.out.println("Error:[" + error.getClass().getName() + "]: " + error.getMessage());
            System.exit(0);
        }

        try {
            connection.setRequestMethod("GET");
        } catch (ProtocolException error) {
            System.out.println("Error:[" + error.getClass().getName() + "]: " + error.getMessage());
            System.exit(0);
        }

        connection.setRequestProperty("User-Agent", "Mozilla/5.0");
        connection.setRequestProperty("cache-control", "no-cache");

        // Getting the response.
        int statusCode = -1;
        try {
            statusCode = connection.getResponseCode();
        } catch (IOException error) {
            System.out.println("Error:[" + error.getClass().getName() + "]: " + error.getMessage());
            System.exit(0);
        }
        System.out.println("[Message Status]: " + statusCode);

        // Closing the scanner object to prevent resource leak.
        scanner.close();

        // Message for successful execution
        System.out.println("Execution successful.");
    }
}